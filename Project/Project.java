/**
* Project -
* 
*  Description of program/solution
* 
* Description of folder content/assignment solution.
* 
* JavaDoc format documentation can be found here: 
* https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html#format
*  
* @author Student Name (SeturStudentID)
* @version 1.0
* @since 2021-03-30
* 	
**/
	
public class Project {
	
	public void main(String[] args) {
		// Content of program
	}
	
}
